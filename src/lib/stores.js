import { browser } from '$app/environment';
import { writable } from 'svelte/store';

function replacer(key, value) {
  if (value instanceof Map) {
    return {
      dataType: 'Map',
      value: Array.from(value.entries()) // or with spread: value: [...value]
    };
  } else {
    return value;
  }
}

function reviver(key, value) {
  if (typeof value === 'object' && value !== null) {
    if (value.dataType === 'Map') {
      return new Map(value.value);
    }
  }
  return value;
}

const getLocalStorage = (key, defaultValue) => {
  if (!browser) {
    return defaultValue;
  }
  let storeValue = window.localStorage.getItem(key);
  if (!storeValue) {
    return defaultValue;
  }
  try {
    return JSON.parse(storeValue, reviver);
  } catch (e) {
    return defaultValue;
  }
};

const subscribeHandler = (key) => {
  return (value) => {
    if (browser) {
      window.localStorage.setItem(key, JSON.stringify(value, replacer));
    }
  };
};

const osDarkMode = () => {
  if (!browser) {
    return false;
  }
  return window.matchMedia('(prefers-color-scheme: dark)').matches;
};

export const darkMode = writable(getLocalStorage('darkMode', osDarkMode()));
darkMode.subscribe(subscribeHandler('darkMode'));
