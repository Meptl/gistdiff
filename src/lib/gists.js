const ID_LEN = 8;

// Expecting this app to only run on a single server. In-memory storage.
let gists = {};

export function createGist(content) {
  const id = generateGistId();
  gists[id] = content;
  return id;
}

export function readGist(id) {
  return gists[id.trim()];
}

function generateRandomString(length) {
  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function generateGistId() {
  var id = generateRandomString(ID_LEN);
  while (gists[id]) {
    id = generateRandomString(ID_LEN);
  }
  return id;
}
