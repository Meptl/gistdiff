import { error } from '@sveltejs/kit';
import { readGist } from '$lib/gists';

export async function load({ params }) {
  const gist = readGist(params.id);
  if (gist) {
    return gist;
  } else {
    error(404, 'Not Found');
  }
}
