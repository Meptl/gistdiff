import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vite';

export default defineConfig({
  optimizeDeps: {
    exclude: ['codemirror', '@codemirror/text', '@codemirror/state', '@codemirror/merge']
  },
  plugins: [sveltekit()]
});
