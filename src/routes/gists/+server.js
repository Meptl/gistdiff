import { json } from '@sveltejs/kit';
import { error } from '@sveltejs/kit';
import { createGist } from '$lib/gists';

export const POST = async ({ request }) => {
  const gist = await request.json();
  const regex = /^https:\/\/github\.com\/(.+?)\/(.+?)\/(.+?)\/(.+?)\/(.+)$/;

  const match = gist.source_url.match(regex);
  if (!match) {
    error(400, 'Bad Request');
  } else {
    const id = createGist(gist);
    return json({ id }, { status: 200 });
  }
};
